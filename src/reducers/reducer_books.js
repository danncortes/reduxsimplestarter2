export default function(){
    return [
        { title: 'Javascript, the good parts', pages: 23},
        { title: 'Harry Potter', pages: 113},
        { title: 'The Dark Tower', pages: 34},
        { title: 'Eloquent Ruby', pages: 1}
    ]
} 